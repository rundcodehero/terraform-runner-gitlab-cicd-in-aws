#!/bin/bash

#install docker
sudo apt-get update
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin 
sudo systemctl start docker 
sudo usermod -aG docker ubuntu
docker version

#intall git
sudo apt-get update
sudo apt-get install git -y
git --version
git config --global user.name "username"
git config --global user.email "email@gmail.com"
git config --list

#install gitlab runner sheell Download and install binary
# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab Runner user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
#GR13.. is token, todo-runner is name runner and describe, shell is type runner
printf 'https://gitlab.com/\nGR1348941TZsQfSrCXHvDhzyzDHVf\ntodo-runner\ntodo-runner\n\nshell\n\n' | sudo gitlab-runner register

sudo usermod -aG docker gitlab-runner

#i use shell, it alwasy error and need rm .bash_logout
sudo rm /home/gitlab-runner/.bash_logout
