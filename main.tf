
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.75"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "todoapps-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
      Name = "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "todoapps-subnet-1" {
  vpc_id = aws_vpc.todoapps-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.avail_zone
  tags = {
      Name = "${var.env_prefix}-subnet-1"
  }
}

resource "aws_security_group" "todoapps-sg" {
  name   = "todoapps-sg"
  vpc_id = aws_vpc.todoapps-vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name = "${var.env_prefix}-sg"
  }
}

resource "aws_internet_gateway" "todoapps-igw" {
	vpc_id = aws_vpc.todoapps-vpc.id
    
    tags = {
     Name = "${var.env_prefix}-internet-gateway"
   }
}

resource "aws_route_table" "todoapps-route-table" {
   vpc_id = aws_vpc.todoapps-vpc.id

   route {
     cidr_block = "0.0.0.0/0"
     gateway_id = aws_internet_gateway.todoapps-igw.id
   }

   # default route, mapping VPC CIDR block to "local", created implicitly and cannot be specified.

   tags = {
     Name = "${var.env_prefix}-route-table"
   }
 }

# Associate subnet with Route Table
resource "aws_route_table_association" "a-rtb-subnet" {
  subnet_id      = aws_subnet.todoapps-subnet-1.id
  route_table_id = aws_route_table.todoapps-route-table.id
}

resource "aws_key_pair" "ssh-key" {
  key_name   = "todoapps-key"
  public_key = file(var.ssh_key)
}

output "server-ip" {
    value = aws_instance.todoapps-server.public_ip
}


 resource "aws_instance" "todoapps-server" {
    ami                         = "ami-09d56f8956ab235b3"
    instance_type               = var.instance_type
    subnet_id                   = aws_subnet.todoapps-subnet-1.id
    vpc_security_group_ids      = [aws_security_group.todoapps-sg.id]
    availability_zone           = var.avail_zone
    associate_public_ip_address = true
    key_name                    = aws_key_pair.ssh-key.key_name
    user_data                   = file("script-gitlab.sh")

    tags = {
        Name = "${var.env_prefix}-server"
    }
}
